from math import sin
import sys

def F(x, y, z):
    return (x & y) | (~x & z)

def G(x, y, z):
    return (x & z) | (~z & y)

def H(x, y, z):
    return x ^ y ^ z

def I(x, y, z):
    return y ^ (~z | x)

ABCDinit = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476]
functions = [F, G, H, I]
T = [int(2 ** 32 * abs(sin(i))) & 0xFFFFFFFF for i in range(1, 65)]

s = [7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
     5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
     4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
     6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21]

index = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
         1, 6, 11, 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12,
         5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15, 2,
         0, 7, 14, 5, 12, 3, 10, 1, 8, 15, 6, 13, 4, 11, 2, 9]

def left_shift(x, amount):
    x &= 0xFFFFFFFF
    return ((x << amount) | (x >> (32 - amount))) & 0xFFFFFFFF

def extend_data(data):
    data_length = (8 * len(data)) & 0xFFFFFFFFFFFFFFFF
    data.append(0x80)

    while len(data) % 64 != 56:
        data.append(0)

    data += data_length.to_bytes(8, sys.byteorder)

def md5(data):
    extend_data(data)

    ABCD = ABCDinit.copy()

    for chunk in [data[i:i + 64] for i in range(0, len(data), 64)]:
        words = [chunk[i:i + 4] for i in range(0, len(chunk), 4)] * 4
        a, b, c, d = ABCD

        for i in range(64):
            F = functions[i//16]
            k = index[i]

            a = (b + left_shift(a + F(b, c, d) + int.from_bytes(words[k], sys.byteorder) + T[i], s[i])) & 0xFFFFFFFF
            a, b, c, d = d, a, b, c

        for i, value in enumerate([a, b, c, d]):
            ABCD[i] = ABCD[i] + value & 0xFFFFFFFF

    raw = sum(x << (32 * i) for i, x in enumerate(ABCD))
    return raw.to_bytes(16, sys.byteorder)


if __name__ == '__main__':
    fh = open('TRPO_TZ.docx', 'rb')
    data = bytearray(fh.read())
    hash = md5(data)
    print(hash.hex())