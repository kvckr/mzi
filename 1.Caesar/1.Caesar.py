alphabetLength = 26
shift = ord('a')

def caesarEncrypt(char, key):
    return chr((ord(char) + key - shift) % alphabetLength + shift)

def caesarDecrypt(char, key):
    return chr((ord(char) - key + alphabetLength - shift) % alphabetLength + shift)

def process(message, key, isEncryption):
    processedString = ""
    for char in message:
        newChar = char
        if (char.isalpha()):
            isUpperCase = char.isupper()
            if isUpperCase: newChar = newChar.lower()
            if (isEncryption):
                newChar = caesarEncrypt(newChar, key)
            else:
                newChar = caesarDecrypt(newChar, key)
            if isUpperCase: newChar = newChar.upper()
        processedString += newChar
    return processedString
    
print(process("ABcDeF AZz!", 2, True))
print(process("ABcDeF AZz!", 28, False))
print(process("ABcDeF AZz!", -2, caesarEncrypt))
print(process("ABcDeF AZz!", -28, caesarEncrypt))
print(process(process("ABcDeF AZz!", 2, True), 2, False))
print(process(process("ABcDeF AZz!", 28, True), 28, False))
print(process(process("ABcDeF AZz!", -2, caesarEncrypt), -2, caesarDecrypt))
print(process(process("ABcDeF AZz!", -28, caesarEncrypt), -28, caesarDecrypt))