import sys
import numpy.random
import itertools
import binascii

sblock = (
    (0x4,0xA,0x9,0x2,0xD,0x8,0x0,0xE,0x6,0xB,0x1,0xC,0x7,0xF,0x5,0x3), # id-GostR3411-94-TestParamSet
    (0x2,0xE,0xB,0x4,0xC,0x6,0xD,0xF,0xA,0x2,0x3,0x8,0x1,0x0,0x7,0x5),
    (0x3,0x5,0x8,0x1,0xD,0xA,0x3,0x4,0x2,0xE,0xF,0xC,0x7,0x6,0x0,0x9),
    (0x4,0x7,0xD,0xA,0x1,0x0,0x8,0x9,0xF,0xE,0x4,0x6,0xC,0xB,0x2,0x5),
    (0x5,0x6,0xC,0x7,0x1,0x5,0xF,0xD,0x8,0x4,0xA,0x9,0xE,0x0,0x3,0xB),
    (0x6,0x4,0xB,0xA,0x0,0x7,0x2,0x1,0xD,0x3,0x6,0x8,0x5,0x9,0xC,0xF),
    (0x7,0xD,0xB,0x4,0x1,0x3,0xF,0x5,0x9,0x0,0xA,0xE,0x7,0x6,0x8,0x2),
    (0x8,0x1,0xF,0xD,0x0,0x5,0x7,0xA,0x4,0x9,0x2,0x3,0xE,0x6,0xB,0x8)
)

def split(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def getSubkeys(key):
    return [(key >> (32 * i)) & 0xFFFFFFFF for i in range(8)]

def f(part, key):
    temp = part ^ key
    output = 0
    for i in range(8):
        output |= ((sblock[i][(temp >> (4 * i)) & 0b1111]) << (4 * i))
    return ((output >> 11) | (output << (32 - 11))) & 0xFFFFFFFF

def decryptRound(B, A, key):
    return A ^ f(B, key), B

def encryptRound(B, A, key):
    return A, B ^ f(A, key)

def encrypt(block, key):
    subkeys = getSubkeys(key)

    B = block >> 32
    A = block & 0xFFFFFFFF

    for i in range(24):
        B, A = encryptRound(B, A, subkeys[i % 8])

    for i in range(8):
        B, A = encryptRound(B, A, subkeys[7 - i])
    return (B << 32) | A

def decrypt(block, key):
    subkeys = getSubkeys(key)

    B = block >> 32
    A = block & 0xFFFFFFFF
    for i in range(8):
        B, A = decryptRound(B, A, subkeys[i])
    for i in range(24):
        B, A = decryptRound(B, A, subkeys[(7 - i) % 8])
    return (B << 32) | A

def ECB_encrypt(blocks, key):
    encryptedBlocks = []
    for block in blocks:
        encryptedBlocks.append(encrypt(block, key))
    return encryptedBlocks

def ECB_decrypt(blocks, key):
    decryptedBlocks = []
    for block in blocks:
        decryptedBlocks.append(decrypt(block, key))
    return decryptedBlocks

def CBC_encrypt(blocks, key, IV):
    firstEncrypted = IV ^ blocks[0]
    encryptedBlocks = [encrypt(firstEncrypted, key)]
    for i, block in enumerate(blocks[1:]):
        encryptedBlocks.append(encrypt(block ^ encryptedBlocks[i], key))
    return encryptedBlocks

def CBC_decrypt(blocks, key, IV):
    firstDecrypted = IV ^ decrypt(blocks[0], key)
    decryptedBlocks = [firstDecrypted]
    for i, block in enumerate(blocks[1:]):
        decryptedBlocks.append(blocks[i] ^ decrypt(blocks[i+1], key))
    return decryptedBlocks

def CFB_encrypt(blocks, key, IV):
    firstEncrypted = encrypt(IV, key) ^ blocks[0]
    encryptedBlocks = [firstEncrypted]
    for i, block in enumerate(blocks[1:]):
        encryptedBlocks.append(encrypt(encryptedBlocks[i], key) ^ blocks[i+1])
    return encryptedBlocks

def CFB_decrypt(blocks, key, IV):
    firstDecrypted = encrypt(IV, key) ^ blocks[0]
    decryptedBlocks = [firstDecrypted]
    for i, block in enumerate(blocks[1:]):
        decryptedBlocks.append(encrypt(blocks[i], key) ^ blocks[i+1])
    return decryptedBlocks

def calculateO(IV, key, count):
    O = [encrypt(IV, key)]
    for i in range(1, count):
        O.append(encrypt(O[i-1], key))
    return O

def OFB(blocks, key, IV):
    O = calculateO(IV, key, len(blocks))
    processedBlocks = []
    for i, el in enumerate(O):
        processedBlocks.append(blocks[i] ^ el)
    return processedBlocks

def messageToBlocks(message):
    return [int(binascii.hexlify(block), 16) for block in split(message, 8)]

def blocksToMessage(blocks):
    try:
        string = b''.join([binascii.unhexlify(hex(block)[2:]) for block in blocks])
        return string
    except:
        print("Cannot convert to string")
        return ""

key = 18318279387912387912789378912379821879387978238793278872378329832982398023031
IV = 8176115190769219961
message = b'hello world!!!!!'
messageBlocks = messageToBlocks(message)
encryptedBlocks = ECB_encrypt(messageBlocks, key)
decryptedBlocks = ECB_decrypt(encryptedBlocks, key)
print(blocksToMessage(encryptedBlocks))
print(blocksToMessage(decryptedBlocks))

messageBlocks = messageToBlocks(message)
encryptedBlocks = CBC_encrypt(messageBlocks, key, IV)
decryptedBlocks = CBC_decrypt(encryptedBlocks, key, IV)
print(blocksToMessage(encryptedBlocks))
print(blocksToMessage(decryptedBlocks))

messageBlocks = messageToBlocks(message)
encryptedBlocks = CFB_encrypt(messageBlocks, key, IV)
decryptedBlocks = CFB_decrypt(encryptedBlocks, key, IV)
print(blocksToMessage(encryptedBlocks))
print(blocksToMessage(decryptedBlocks))

messageBlocks = messageToBlocks(message)
encryptedBlocks = OFB(messageBlocks, key, IV)
decryptedBlocks = OFB(encryptedBlocks, key, IV)
print(blocksToMessage(encryptedBlocks))
print(blocksToMessage(decryptedBlocks))