import random
import math
import sys
from Cryptodome.Util import number
import binascii

def rsa_process(message, key):
    return pow(message, key[0], key[1])

def greatest_common_divisor(a, b):
    while b != 0:
        a, b = b, a % b
    return a

def extended_greatest_common_divisor(aa, bb):
    lastremainder, remainder = abs(aa), abs(bb)
    x, lastx, y, lasty = 0, 1, 1, 0
    while remainder:
        lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
        x, lastx = lastx - quotient*x, x
        y, lasty = lasty - quotient*y, y
    return lastremainder, lastx * (-1 if aa < 0 else 1), lasty * (-1 if bb < 0 else 1)
 
def modinv(a, m):
    g, x, y = extended_greatest_common_divisor(a, m)
    if g != 1:
        raise ValueError
    return x % m

def rsa_generate_keys(p, q):
    n = p * q
    phi = (p - 1) * (q - 1)

    e = 17
    while greatest_common_divisor(e, phi) != 1:
        e += 1

    d = modinv(e, phi)
    return (e, n), (d, n)

def message_to_number(message):
    return int(binascii.hexlify(message), 16)

def number_to_message(number):
    try:
        message = binascii.unhexlify(hex(number)[2:])
    except:
        message = "failed string convertion"
    return message

message = b'hello world'
num = message_to_number(message)
n_length = 1024

p = number.getPrime(n_length)
q = number.getPrime(n_length)

public, private = rsa_generate_keys(p, q)
#print(f"Public key: {public}\nPrivate key: {private}")

encrypted = rsa_process(num, public)
decrypted = rsa_process(encrypted, private)

print(message)
print(number_to_message(encrypted))
print(number_to_message(decrypted))
